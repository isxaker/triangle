﻿using System;

namespace Triangle
{

    /// <summary>
    ///     anything that has square
    /// </summary>
    public interface ISquare
    {

        int Square();

    }

    /// <summary>
    ///     a shape with square
    /// </summary>
    public abstract class Shape : ISquare
    {

        public abstract int Square();

    }

    /// <summary>
    ///     base abstract triangle that has a sqaure that calculated by geron algoright
    /// </summary>
    public abstract class BaseTriangle : Shape
    {
        #region  fields

        protected int A;
        protected int B;
        protected int C;

        #endregion

        #region  constructors

        protected BaseTriangle(int a, int b, int c)
        {
            if (a <= 0 || b <= 0 || c <= 0)
            {
                throw new ArgumentException("Triangle side length must greater than 0");
            }

            this.A = a;
            this.B = b;
            this.C = c;
        }

        #endregion

        protected int GeronSquare(int a, int b, int c)
        {
            int p = a + b + c;
            return (int) Math.Sqrt(p*(p - a)*(p - b)*(p - c));
        }

    }

    public class Triangle : BaseTriangle
    {
        #region  constructors

        public Triangle(int a, int b, int c) : base(a, b, c)
        {
        }

        #endregion

        public virtual int Square(int a, int b, int c)
        {
            return base.GeronSquare(a, b, c);
        }

        public override int Square()
        {
            return this.Square(this.A, this.B, this.C);
        }

    }

    /// <summary>
    ///     Rectangular triangle implementation
    /// </summary>
    public sealed class RectangularTriangle : Triangle
    {
        #region  constructors

        public RectangularTriangle(int a, int b, int c) : base(a, b, c)
        {
            if (!RectangularTriangle.IsRectangular(a, b, c))
            {
                throw new ArgumentException("Passed parameters not satisfied of Rectangular Triangle law! a*a + b*b == c*c");
            }
        }

        #endregion

        #region static

        private static bool IsRectangular(int a, int b, int c)
        {
            return a*a + b*b == c*c;
        }

        #endregion

        public override int Square()
        {
            return this.Square(this.A, this.B, this.C);
        }

        /// <summary>
        ///     Claculate RectangularTriangle's square
        /// </summary>
        /// <param name="a">cathetus</param>
        /// <param name="b">cathetus</param>
        /// <param name="c">hypotenuse</param>
        /// <returns>square of the RectangularTriangle</returns>
        public override int Square(int a, int b, int c)
        {
            int p = (a + b + c)/2;
            return (p - a)*(p - b);
        }

    }

}